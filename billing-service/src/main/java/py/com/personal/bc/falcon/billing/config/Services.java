package py.com.personal.bc.falcon.billing.config;

import py.com.personal.bc.falcon.billing.services.OperacionesLinea;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alfredo Barrios
 * @since 1.0
 */
@ApplicationPath("/billing")
public class Services extends Application {

    private Set<Object> singletons = new HashSet<>();

    public Services() {
        singletons.add(new OperacionesLinea());
    }

    @Override
    public Set<Object> getSingletons() {
        return singletons;
    }
}

