package py.com.personal.bc.falcon.billing.services;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.bussines.CreditoBusiness;
import py.com.personal.bc.falcon.billing.bussines.MiddlewareBusiness;
import py.com.personal.bc.falcon.billing.model.dto.*;

import javax.inject.Inject;
import javax.validation.Valid;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * @author Alfredo Barrios
 * @since 1.0
 * <p>
 * Operation Services over each line number
 **/

@Path("/linea")
@Consumes({MediaType.APPLICATION_JSON})
@Produces({MediaType.APPLICATION_JSON})
public class OperacionesLinea {

    private static final Logger logger = LoggerFactory.getLogger(MiddlewareBusiness.class);

    @Inject
    private MiddlewareBusiness middlewareBusiness;

    @Inject
    private CreditoBusiness creditoBusiness;

    @POST
    @Path("/activar")
    public Response activarLinea(@Valid LineaDTO linea) {


        try {

            ResponseGeneric response = middlewareBusiness.activate(linea.getLinea(), linea.getPerfilId(), linea.getPlan());

            logger.info("Activar Linea Response: {}", response);

            return Response.status(Response.Status.OK).entity(response).build();

        }catch (Exception e){
            logger.error("Controller Activar Linea : {}", e.getMessage());
            logger.debug("Controller activar Linea : {}", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @DELETE
    @Path("/desactivar")
    public Response desactivarLinea(@QueryParam("linea") String linea) {

        try {

            ResponseGeneric response = middlewareBusiness.deactivate(linea);

            logger.info("Activar Linea Response: {}", response);

            return Response.status(Response.Status.OK).entity(response).build();

        }catch (Exception e){
            logger.error("Controller Desactivar Linea : {}", e.getMessage());
            logger.debug("Controller Desactivar Linea : {}", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }


    @POST
    @Path("/credito")
    public Response creditoLinea(MovimientosLineaDTO movimiento) {


        try {

            ResponseGeneric response = creditoBusiness.acreditar(movimiento);

            logger.info("Acreditar Linea Response: {}", response);

            return Response.status(Response.Status.OK).entity(response).build();

        }catch (Exception e){
            logger.error("Controller Activar Linea : {}", e.getMessage());
            logger.debug("Controller activar Linea : {}", e);
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

    }


    @POST
    @Path("/debito")
    public Response debitoLinea(MovimientosLineaDTO movimiento) {

        return Response.status(Response.Status.OK).build();

    }


    @GET
    public LineaResponseDTO getLinea(@QueryParam("linea") String linea) {
        return null;
    }


    @GET
    @Path("/detalle")
    public LineaDetalleResponseDTO getLineaDetalle(@QueryParam("linea") String linea) {
        return null;
    }

}
