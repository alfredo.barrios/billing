package py.com.personal.bc.falcon.billing.model;


import py.com.personal.bc.voltdb.utils.annotations.Column;
import py.com.personal.bc.voltdb.utils.annotations.ID;
import py.com.personal.bc.voltdb.utils.annotations.PartitionKey;
import py.com.personal.bc.voltdb.utils.annotations.Table;

import java.util.Date;
/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */
@ID({ "LINEA" })
@Table("AB_HISTORIAL_OPERACIONES")
public class Historial {

    public static class Columnas {
        public static final String LINEA = "LINEA";
        public static final String TIPO_CREDITO = "TIPO_CREDITO";
        public static final String PROCESO = "PROCESO";
        public static final String FECHA = "FECHA";
        public static final String PLAN = "PLAN";
        public static final String PERFIL_ORIGINAL = "PERFIL_ORIGINAL";
        public static final String PERFIL_FINAL = "PERFIL_FINAL";
        public static final String CREDITO_ORIGINAL = "CREDITO_ORIGINAL";
        public static final String CREDITO_FINAL = "CREDITO_FINAL";
        public static final String DATOS_ORIGINAL = "DATOS_ORIGINAL";
        public static final String DATOS_FINAL = "DATOS_FINAL";
        public static final String VOZ_ORIGINAL = "VOZ_ORIGINAL";
        public static final String VOZ_FINAL = "VOZ_FINAL";
        public static final String SMS_ORIGINAL = "SMS_ORIGINAL";
        public static final String SMS_FINAL = "SMS_FINAL";
        public static final String TRANSACCION = "TRANSACCION";
    }

    @PartitionKey
    @Column( value = Historial.Columnas.LINEA, order = 1)
    private  String linea;

    @Column( value = Historial.Columnas.TIPO_CREDITO, order = 2)
    private  String tipoCredito;

    @Column( value = Historial.Columnas.PROCESO, order = 3)
    private  String proceso;

    @Column( value = Historial.Columnas.FECHA, order = 4)
    private Date fecha;


    @Column( value = Historial.Columnas.PLAN, order = 5)
    private  String plan;

    @Column( value = Historial.Columnas.PERFIL_ORIGINAL, order = 6)
    private  String perfilOriginal;


    @Column( value = Historial.Columnas.PERFIL_FINAL, order = 7)
    private  String perfilFinal;


    @Column( value = Historial.Columnas.CREDITO_ORIGINAL, order = 8)
    private  Double creditoOriginal;


    @Column( value = Historial.Columnas.CREDITO_FINAL, order = 9)
    private  Double creditoFinal;


    @Column( value = Historial.Columnas.DATOS_ORIGINAL, order = 10)
    private  Double datosOriginal;


    @Column( value = Historial.Columnas.DATOS_FINAL, order = 11)
    private  Double datosFinal;



    @Column( value = Historial.Columnas.VOZ_ORIGINAL, order = 12)
    private  Double vozOriginal;


    @Column( value = Historial.Columnas.VOZ_FINAL, order = 13)
    private  Double vozFinal;


    @Column( value = Historial.Columnas.SMS_ORIGINAL, order = 14)
    private  Double smsOriginal;


    @Column( value = Historial.Columnas.SMS_FINAL, order = 15)
    private  Double smsFinal;

    @Column( value = Historial.Columnas.TRANSACCION, order = 16)
    private Long transaccion;

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public String getProceso() {
        return proceso;
    }

    public void setProceso(String proceso) {
        this.proceso = proceso;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getPerfilOriginal() {
        return perfilOriginal;
    }

    public void setPerfilOriginal(String perfilOriginal) {
        this.perfilOriginal = perfilOriginal;
    }

    public String getPerfilFinal() {
        return perfilFinal;
    }

    public void setPerfilFinal(String perfilFinal) {
        this.perfilFinal = perfilFinal;
    }

    public Double getCreditoOriginal() {
        return creditoOriginal;
    }

    public void setCreditoOriginal(Double creditoOriginal) {
        this.creditoOriginal = creditoOriginal;
    }

    public Double getCreditoFinal() {
        return creditoFinal;
    }

    public void setCreditoFinal(Double creditoFinal) {
        this.creditoFinal = creditoFinal;
    }

    public Double getDatosOriginal() {
        return datosOriginal;
    }

    public void setDatosOriginal(Double datosOriginal) {
        this.datosOriginal = datosOriginal;
    }

    public Double getDatosFinal() {
        return datosFinal;
    }

    public void setDatosFinal(Double datosFinal) {
        this.datosFinal = datosFinal;
    }

    public Double getVozOriginal() {
        return vozOriginal;
    }

    public void setVozOriginal(Double vozOriginal) {
        this.vozOriginal = vozOriginal;
    }

    public Double getVozFinal() {
        return vozFinal;
    }

    public void setVozFinal(Double vozFinal) {
        this.vozFinal = vozFinal;
    }

    public Double getSmsOriginal() {
        return smsOriginal;
    }

    public void setSmsOriginal(Double smsOriginal) {
        this.smsOriginal = smsOriginal;
    }

    public Double getSmsFinal() {
        return smsFinal;
    }

    public void setSmsFinal(Double smsFinal) {
        this.smsFinal = smsFinal;
    }

    public Long getTransaccion() {
        return transaccion;
    }

    public void setTransaccion(Long transaccion) {
        this.transaccion = transaccion;
    }

    @Override
    public String toString() {
        return "HistorialOperaciones{" +
                "linea='" + linea + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", proceso='" + proceso + '\'' +
                ", fecha=" + fecha +
                ", plan='" + plan + '\'' +
                ", perfilOriginal='" + perfilOriginal + '\'' +
                ", perfilFinal='" + perfilFinal + '\'' +
                ", creditoOriginal=" + creditoOriginal +
                ", creditoFinal=" + creditoFinal +
                ", datosOriginal=" + datosOriginal +
                ", datosFinal=" + datosFinal +
                ", vozOriginal=" + vozOriginal +
                ", vozFinal=" + vozFinal +
                ", smsOriginal=" + smsOriginal +
                ", smsFinal=" + smsFinal +
                ", transaccion=" + transaccion +
                '}';
    }
}
