package py.com.personal.bc.falcon.billing.model;

import py.com.personal.bc.voltdb.utils.annotations.Column;
import py.com.personal.bc.voltdb.utils.annotations.ID;
import py.com.personal.bc.voltdb.utils.annotations.Table;
/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */


@Table("AB_PERFILES")
@ID({
  Perfil.Columnas.PERFIL
})
public class Perfil {

    public static class Columnas {
        public static final String CREDITO = "CREDITO";
        public static final String VOZ = "VOZ";
        public static final String SMS = "SMS";
        public static final String DATOS = "DATOS";
        public static final String PERFIL = "PERFIL";

    }

    @Column( value = Perfil.Columnas.CREDITO, order = 1)
    private  String credito;

    @Column( value = Perfil.Columnas.VOZ, order = 2)
    private  String voz;

    @Column( value = Perfil.Columnas.SMS, order = 3)
    private  String sms;

    @Column( value = Perfil.Columnas.DATOS, order = 4)
    private  String datos;

    @Column( value = Perfil.Columnas.PERFIL, order = 5)
    private  String perfil;

    public String getCredito() {
        return credito;
    }

    public void setCredito(String credito) {
        this.credito = credito;
    }

    public String getVoz() {
        return voz;
    }

    public void setVoz(String voz) {
        this.voz = voz;
    }

    public String getSms() {
        return sms;
    }

    public void setSms(String sms) {
        this.sms = sms;
    }

    public String getDatos() {
        return datos;
    }

    public void setDatos(String datos) {
        this.datos = datos;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    @Override
    public String toString() {
        return "Perfiles{" +
                "credito='" + credito + '\'' +
                ", voz='" + voz + '\'' +
                ", sms='" + sms + '\'' +
                ", datos='" + datos + '\'' +
                ", perfil='" + perfil + '\'' +
                '}';
    }
}
