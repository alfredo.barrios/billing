package py.com.personal.bc.falcon.billing.model.dto;

public class MovimientosLineaDTO {

    String linea;
    String tipoCredito;
    Double cantidad;

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    @Override
    public String toString() {
        return "MovimientosLinea{" +
                "linea='" + linea + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", cantidad=" + cantidad +
                '}';
    }
}
