package py.com.personal.bc.falcon.billing.model.dto;


/**
 * @author Alfredo Barrios
 * @since 1.0
 */
public class LineaDTO {

    String linea;

    String perfilId;

    String plan;

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    public String getPerfilId() {
        return perfilId;
    }

    public void setPerfilId(String perfilId) {
        this.perfilId = perfilId;
    }

    @Override
    public String toString() {
        return "LineaDTO{" +
                "linea='" + linea + '\'' +
                ", perfilId='" + perfilId + '\'' +
                ", plan='" + plan + '\'' +
                '}';
    }
}
