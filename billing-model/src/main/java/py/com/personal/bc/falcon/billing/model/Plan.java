package py.com.personal.bc.falcon.billing.model;

import py.com.personal.bc.voltdb.utils.annotations.Column;
import py.com.personal.bc.voltdb.utils.annotations.ID;
import py.com.personal.bc.voltdb.utils.annotations.PartitionKey;
import py.com.personal.bc.voltdb.utils.annotations.Table;
/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */


@Table("AB_PLANES")
@ID({
   Plan.Columnas.LINEA
})
public class Plan {

    public static class Columnas {
        public static final String LINEA = "LINEA";
        public static final String PLAN = "PLAN";

    }
    @PartitionKey
    @Column( value = Plan.Columnas.LINEA, order = 1)
    private  String linea;

    @Column( value = Plan.Columnas.PLAN, order = 2)
    private  String plan;


    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getPlan() {
        return plan;
    }

    public void setPlan(String plan) {
        this.plan = plan;
    }

    @Override
    public String toString() {
        return "Planes{" +
                "linea='" + linea + '\'' +
                ", plan='" + plan + '\'' +
                '}';
    }
}
