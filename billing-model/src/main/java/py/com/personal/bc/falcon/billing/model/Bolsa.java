package py.com.personal.bc.falcon.billing.model;

import py.com.personal.bc.voltdb.utils.annotations.Column;
import py.com.personal.bc.voltdb.utils.annotations.ID;
import py.com.personal.bc.voltdb.utils.annotations.PartitionKey;
import py.com.personal.bc.voltdb.utils.annotations.Table;

import java.util.Date;

/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */
@Table("AB_BOLSA")
@ID({
        Bolsa.Columnas.LINEA,
        Bolsa.Columnas.TIPO_CREDITO
})
public class Bolsa {

    public static class Columnas {
        public static final String LINEA = "LINEA";
        public static final String TIPO_CREDITO = "TIPO_CREDITO";
        public static final String CANTIDAD = "cantidad";
        public static final String FECHA_ACTUALIZACION = "FECHA_ACTUALIZACION";
        public static final String ULTIMA_TRANSACCION = "ULTIMA_TRANSACCION";
        public static final String ULTIMO_PROCESO = "ULTIMO_PROCESO";

    }

    @PartitionKey
    @Column( value = Columnas.LINEA, order = 1)
    private  String linea;

    @Column( value = Columnas.TIPO_CREDITO, order = 2)
    private  String tipoCredito;

    @Column( value = Columnas.CANTIDAD, order = 3)
    private  Double cantidad;

    @Column( value = Columnas.FECHA_ACTUALIZACION, order = 4)
    private Date fechaActualizacion;

    @Column( value = Columnas.ULTIMA_TRANSACCION, order = 5)
    private Long ultimaTransaccion;

    @Column( value = Columnas.ULTIMO_PROCESO, order = 6)
    private  String ultimoProceso;


    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Double getCantidad() {
        return cantidad;
    }

    public void setCantidad(Double cantidad) {
        this.cantidad = cantidad;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    public Long getUltimaTransaccion() {
        return ultimaTransaccion;
    }

    public void setUltimaTransaccion(Long ultimaTransaccion) {
        this.ultimaTransaccion = ultimaTransaccion;
    }

    public String getUltimoProceso() {
        return ultimoProceso;
    }

    public void setUltimoProceso(String ultimoProceso) {
        this.ultimoProceso = ultimoProceso;
    }

    @Override
    public String toString() {
        return "Bolsa{" +
                "linea='" + linea + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", cantidad=" + cantidad +
                ", fechaActualizacion=" + fechaActualizacion +
                ", ultimaTransaccion=" + ultimaTransaccion +
                ", ultimoProceso='" + ultimoProceso + '\'' +
                '}';
    }
}
