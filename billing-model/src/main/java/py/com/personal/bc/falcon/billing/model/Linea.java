package py.com.personal.bc.falcon.billing.model;

import py.com.personal.bc.voltdb.utils.annotations.*;
import py.com.personal.bc.voltdb.utils.mapper.views.DefaultView;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */

@Table("AB_LINEA")
@ID({
  Linea.Columnas.LINEA
})
public class Linea {


    public static class Columnas {
        public static final String LINEA = "LINEA";
        public static final String PERFIL = "PERFIL";
        public static final String ACTUALIZACION_PERFIL = "ACTUALIZACION_PERFIL";
    }

    public static class Relaciones {
        public static final String BOLSA = "BOLSA";
        public static final String PLANES = "PLANES";
    }


    @PartitionKey
    @Column( value = Linea.Columnas.LINEA, order = 1)
    private  String linea;

    @Column( value = Linea.Columnas.PERFIL, order = 2)
    private  String perfil;

    @Column( value = Linea.Columnas.ACTUALIZACION_PERFIL, order = 3)
    private Date fechaActualizacion;

    @IncludeWithView(DefaultView.class)
    @Relation(value = Relaciones.BOLSA, joinColumns = Columnas.LINEA)
    private List<Bolsa> creditos = new ArrayList<>();

    @IncludeWithView(DefaultView.class)
    @IncludeWithView(SaldoView.class)
    @Relation(value = Relaciones.PLANES, joinColumns = Columnas.LINEA)
    private Plan plan;


    public List<Bolsa> getCreditos() {
        return creditos;
    }

    public void setCreditos(List<Bolsa> creditos) {
        this.creditos = creditos;
    }

    public Plan getPlan() {
        return plan;
    }

    public void setPlan(Plan plan) {
        this.plan = plan;
    }

    public String getLinea() {
        return linea;
    }

    public void setLinea(String linea) {
        this.linea = linea;
    }

    public String getPerfil() {
        return perfil;
    }

    public void setPerfil(String perfil) {
        this.perfil = perfil;
    }

    public Date getFechaActualizacion() {
        return fechaActualizacion;
    }

    public void setFechaActualizacion(Date fechaActualizacion) {
        this.fechaActualizacion = fechaActualizacion;
    }

    @Override
    public String toString() {
        return "Linea{" +
                "linea='" + linea + '\'' +
                ", perfil='" + perfil + '\'' +
                ", fechaActualizacion=" + fechaActualizacion +
                ", creditos=" + creditos +
                '}';
    }
}
