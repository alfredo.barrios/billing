package py.com.personal.bc.falcon.billing.model.dto;
public class ErrorsDTO {

        public enum CODE {

             NOTAUTHORIZED("0001"), NULLVALUES("0002"), NOEXIST("0003"), INVALID_AMOUNT("0004")
                ,INSUFFICIENT_CREDIT("0004");
            private final String rule;

            CODE(String rule) { this.rule = rule; }

            public String getValue() { return rule; }
        }

        private String errorCode;

        private String message;

        public ErrorsDTO(String errorCode, String message) {
            this.errorCode = errorCode;
            this.message = message;
        }

        public ErrorsDTO(String errorCode) {
            this.errorCode = errorCode;
        }

        public String getErrorCode() {
            return errorCode;
        }

        public String getMessage() {
            return message;
        }

        public void setErrorCode(String errorCode) {
            this.errorCode = errorCode;
        }

        public void setMessage(String message) {
            this.message = message;
        }

}
