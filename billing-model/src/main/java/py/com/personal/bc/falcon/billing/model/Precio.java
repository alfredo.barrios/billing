package py.com.personal.bc.falcon.billing.model;

import py.com.personal.bc.voltdb.utils.annotations.Column;
import py.com.personal.bc.voltdb.utils.annotations.ID;
import py.com.personal.bc.voltdb.utils.annotations.Table;

/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * */

@Table("AB_PRECIOS")
@ID({
    Precio.Columnas.PLAN_ID,
        Precio.Columnas.TIPO_CREDITO
})
public class Precio {

    public static class Columnas {
        public static final String PLAN_ID = "PLAN_ID";
        public static final String TIPO_CREDITO = "TIPO_CREDITO";
        public static final String PRECIO = "PRECIO";
    }

    @Column( value = Precio.Columnas.PLAN_ID, order = 1)
    private  String planId;

    @Column( value = Precio.Columnas.TIPO_CREDITO, order = 2)
    private  String tipoCredito;

    @Column( value = Precio.Columnas.PRECIO, order = 3)
    private Double precios;


    public String getPlanId() {
        return planId;
    }

    public void setPlanId(String planId) {
        this.planId = planId;
    }

    public String getTipoCredito() {
        return tipoCredito;
    }

    public void setTipoCredito(String tipoCredito) {
        this.tipoCredito = tipoCredito;
    }

    public Double getPrecios() {
        return precios;
    }

    public void setPrecios(Double precios) {
        this.precios = precios;
    }

    @Override
    public String toString() {
        return "Precios{" +
                "planId='" + planId + '\'' +
                ", tipoCredito='" + tipoCredito + '\'' +
                ", precios=" + precios +
                '}';
    }
}
