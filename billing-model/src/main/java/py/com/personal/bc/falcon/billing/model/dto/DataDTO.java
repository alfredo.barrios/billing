package py.com.personal.bc.falcon.billing.model.dto;

import java.util.Map;

public class DataDTO {

    public enum CODE {
        OK("00"), CREATE_SUCCESS("001"), UPDATE_SUCCESS("002"), DELETE_SUCCESS("003"), NO_DATA("004");

        private final String rule;

        CODE(String rule) { this.rule = rule; }

        public String getValue() { return rule; }
    }
    private String dataCode;
    private String message;
    private Map<String,Object> body;

    public String getDataCode() {
        return dataCode;
    }

    public void setDataCode(String dataCode) {
        this.dataCode = dataCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Map<String, Object> getBody() {
        return body;
    }

    public void setBody(Map<String, Object> body) {
        this.body = body;
    }

}
