package py.com.personal.bc.falcon.billing.model.dto;
/**
 * @author Alfredo Barrios
 * @since 1.0
 *
 * Simple Response
 * **/
public class ResponseGeneric {
    private boolean status;
    private String statusCode;
    private String description;
    private DataDTO data;


    public ResponseGeneric(boolean status, String statusCode, String description) {
        this.status = status;
        this.statusCode = statusCode;
        this.description = description;
        this.data = null;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public DataDTO getData() {
        return data;
    }

    public void setData(DataDTO data) {
        this.data = data;
    }

    @Override
    public String toString() {
        return "Success{" +
                "status=" + status +
                ", statusCode=" + statusCode +
                ", description='" + description + '\'' +
                '}';
    }
}
