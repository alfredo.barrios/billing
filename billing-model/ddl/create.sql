CREATE TABLE AB_BOLSA(

    linea VARCHAR (12),
    tipo_credito VARCHAR (1),
    cantidad DECIMAL,
    fecha_actualizacion TIMESTAMP,
    ultima_transaccion BIGINT,
    ultimo_proceso varchar(20),
    CONSTRAINT AB_PRIKEY_BOLSA PRIMARY KEY (linea,tipo_credito)

);
PARTITION TABLE AB_BOLSA ON COLUMN linea;

CREATE TABLE AB_LINEA(
    linea VARCHAR(12),
    perfil VARCHAR(10),
    actualizacion_perfil TIMESTAMP,
    CONSTRAINT AB_PRIKEY_LINEA PRIMARY KEY (linea)
);
PARTITION TABLE AB_LINEA ON COLUMN linea;

CREATE TABLE AB_PLANES(
    linea VARCHAR(12),
    plan VARCHAR(2),
    CONSTRAINT AB_PRIKEY_PLANES PRIMARY KEY (linea)
);
PARTITION TABLE AB_PLANES ON COLUMN linea;


CREATE TABLE AB_PRECIOS(
    plan_id VARCHAR(2),
    tipo_credito VARCHAR(1),
    precio DECIMAL,
    CONSTRAINT AB_PRIKEY_PLANES PRIMARY KEY (plan_id,tipo_credito)
);

CREATE TABLE AB_PERFILES(
    credito VARCHAR(1),
    voz VARCHAR(1),
    sms VARCHAR(1),
    datos VARCHAR(1),
    perfil VARCHAR(10),
    CONSTRAINT AB_PRIKEY_PERFILES PRIMARY KEY (perfil)
);

CREATE TABLE AB_HISTORIAL_OPERACIONES(
   linea VARCHAR(12),
   tipo_credito VARCHAR(1),
   proceso VARCHAR(20),
   fecha TIMESTAMP,
   plan VARCHAR(2),
   perfil_original VARCHAR(10),
   perfil_final VARCHAR(10),
   credito_original DECIMAL,
   credito_final DECIMAL,
   datos_original DECIMAL,
   datos_final DECIMAL,
   voz_original DECIMAL,
   voz_final DECIMAL,
   sms_original DECIMAL,
   sms_final DECIMAL,
   transaccion BIGINT,
   CONSTRAINT AB_PRIKEY_HISTORIAL_OPERACIONES PRIMARY KEY (linea)
);
PARTITION TABLE AB_HISTORIAL_OPERACIONES ON COLUMN linea;
