package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.BolsaDAO;
import py.com.personal.bc.falcon.billing.dao.BolsaDAOTrx;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.exceptions.ObjectAlreadyExist;
import py.com.personal.bc.falcon.billing.model.Bolsa;


import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Date;
import java.util.List;

@ApplicationScoped
public class BolsaBusiness {

    private static final Logger logger = LoggerFactory.getLogger(BolsaBusiness.class);

    @Inject
    private BolsaDAO dao;

    @Inject
    private BolsaDAOTrx daoTrx;

    public List<Bolsa> loadBlocking(String lineaId) throws Exception{

        try {

            return daoTrx.loadByLinea(lineaId);

        }catch (Exception e){
            logger.error("load: {} ", e.getMessage());
            logger.debug("load ", e);
            throw e;
        }
    }

    public Bolsa create(String linea, String tipoCredito) throws Exception {

        if (linea == null || tipoCredito == null) {
            logger.error("Linea : {} , tipo credito: {}", linea, tipoCredito);
            throw new CRUDFail("BOLSA_CREATE : NULL VALUES");
        }

        Bolsa checkBolsa = dao.select(linea, tipoCredito);

        if (checkBolsa != null) {
            logger.error("Bolsa Already Exist");
            throw new ObjectAlreadyExist("BolsaId: " + linea + " tipoCredito: " + tipoCredito);
        }

        try {

            Bolsa bolsa = new Bolsa();
            bolsa.setLinea(linea);
            bolsa.setTipoCredito(linea);
            bolsa.setFechaActualizacion(new Date());
            bolsa.setUltimoProceso("CREATE");
            daoTrx.insert(bolsa);

            return bolsa;

        } catch (Exception e) {
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }

    public Bolsa update(Bolsa bolsa) throws Exception{

        if (bolsa == null) {
            logger.error("Bolsa -> UPDATE NULL");
            throw new CRUDFail("Updating  Bolsa-->NULL");
        }

        try {

            daoTrx.update(bolsa);

            return bolsa;

        } catch (Exception e) {

            logger.error("update: {} ", e.getMessage());
            logger.debug("update ", e);
            throw e;
        }

    }

    public Bolsa delete(Bolsa bolsa) throws Exception {

        if (bolsa == null ){
            logger.error("Bolsa -> DELETE NULL");
            throw new CRUDFail("Deleting  Bolsa-->NULL");
        }


        try {

            daoTrx.delete(bolsa);

            return bolsa;

        } catch (Exception e) {

            logger.error("delete bolsa: {} ", e.getMessage());
            logger.debug("delete bolsa ", e);
            throw e;
        }
    }

}
