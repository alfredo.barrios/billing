package py.com.personal.bc.falcon.billing.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.common.transactions.voltdb.dao.TransactionalDAO;
import py.com.personal.bc.falcon.billing.model.Perfil;
import py.com.personal.bc.voltdb.utils.mapper.MapConfig;

public class PerfilDAOTrx extends TransactionalDAO<Perfil> {

    private static final Logger logger = LoggerFactory.getLogger(PerfilDAOTrx.class);


    public Perfil load(String perfilId) throws Exception {

        Perfil perfil= new Perfil();

        perfil.setPerfil(perfilId);

        MapConfig config = new MapConfig(Perfil.class, null, true);

        String anyPartition = Math.random() + "";

        return singlePartitionedLoad(perfil, config, anyPartition, null);
    }

}
