package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.PrecioDAO;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.exceptions.ObjectAlreadyExist;
import py.com.personal.bc.falcon.billing.model.Precio;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PrecioBusiness {
    private static final Logger logger = LoggerFactory.getLogger(BolsaBusiness.class);

    @Inject
    private PrecioDAO dao;

    public Precio create(String planId, String tipoCredito) throws Exception{

        if(planId == null || tipoCredito == null)
        {
            logger.error("PlanId or TipoCredito NULL");
            throw new CRUDFail("Precio_CREATE : NULL VALUES");

        }
        if (dao.select(planId,tipoCredito) != null) {
            logger.error("Error al insertar  , el planId {}, tipoCredito {}  ya existe:",  planId, tipoCredito);
            throw new ObjectAlreadyExist("PlanId: " + planId +", TipoCredito: "+tipoCredito);
        }

        try {

            Precio precio = new Precio();
            precio.setPlanId(planId);
            precio.setTipoCredito(tipoCredito);
            dao.insert(precio);

            return precio;

        } catch (Exception e) {
            logger.error("create precio: {} ", e.getMessage());
            logger.debug("create precio: ", e);
            throw e;
        }
    }

    public Precio update(Precio precio) throws Exception{
        if (precio == null)
            throw new CRUDFail("Updating Precio NULL");

        try {

            dao.update(precio);

            return precio;

        } catch (Exception e) {

            logger.error("update precio: {} ", e.getMessage());
            logger.debug("update precio", e);
            throw e;
        }
    }

    public Precio delete(Precio precio) throws Exception{

        if (precio == null)
            throw new CRUDFail("Deleting Precio NULL");

        try {

            dao.delete(precio);

            return precio;

        } catch (Exception e) {

            logger.error("delete precio: {} ", e.getMessage());
            logger.debug("delete precio", e);
            throw e;
        }
    }
}
