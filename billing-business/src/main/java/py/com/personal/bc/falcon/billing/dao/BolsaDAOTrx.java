package py.com.personal.bc.falcon.billing.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.common.transactions.voltdb.util.loader.LoadSettings;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.model.Bolsa;
import py.com.personal.bc.common.transactions.voltdb.dao.TransactionalDAO;
import py.com.personal.bc.voltdb.utils.mapper.MapConfig;
import py.com.personal.bc.voltdb.utils.mapper.views.DefaultView;

import java.util.List;

public class BolsaDAOTrx extends TransactionalDAO<Bolsa> {

    private static final Logger logger = LoggerFactory.getLogger(BolsaDAOTrx.class);

   public List<Bolsa> loadByLinea(String linea) throws Exception {

        Bolsa bolsa = new Bolsa();
        bolsa.setLinea(linea);

        MapConfig config = new MapConfig(Bolsa.class, null, true);
        config.setLock(true);

        return singlePartitionedLoadList(bolsa, config, linea, null);
    }


    public Bolsa load(String linea, String tipoCredito) throws Exception {


        Bolsa bolsaReturn;

        MapConfig config = new MapConfig(Bolsa.class, null, true);
        config.setLock(true);

        try {
            Bolsa bolsa = new Bolsa();
            bolsa.setLinea(linea);
            bolsa.setTipoCredito(tipoCredito);

            bolsaReturn = this.singlePartitionedLoad(bolsa, config,linea,null);

        } catch (Exception e) {
            logger.error("BolsaDAO {}", e.getMessage());
            logger.debug("BolsaDAO {}",e);
            throw new CRUDFail(e.getMessage());
        }
        return bolsaReturn;
    }

}
