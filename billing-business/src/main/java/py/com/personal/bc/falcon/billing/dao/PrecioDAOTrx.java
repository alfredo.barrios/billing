package py.com.personal.bc.falcon.billing.dao;

import py.com.personal.bc.common.transactions.voltdb.dao.TransactionalDAO;
import py.com.personal.bc.falcon.billing.model.Precio;
import py.com.personal.bc.voltdb.utils.mapper.MapConfig;

import java.util.List;

public class PrecioDAOTrx extends TransactionalDAO<Precio> {

    public List<Precio> load(String planId) throws Exception {

        Precio precio= new Precio();

        precio.setPlanId(planId);

        MapConfig config = new MapConfig(Precio.class, null, true);

        String anyPartition = Math.random() + "";

        return singlePartitionedLoadList(precio, config, anyPartition, null);
    }
}
