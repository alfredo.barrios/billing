package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.common.transactions.TransactionModes;
import py.com.personal.bc.common.transactions.Transactional;
import py.com.personal.bc.falcon.billing.model.Bolsa;
import py.com.personal.bc.falcon.billing.model.Linea;
import py.com.personal.bc.falcon.billing.model.dto.DataDTO;
import py.com.personal.bc.falcon.billing.model.dto.ErrorsDTO;
import py.com.personal.bc.falcon.billing.model.dto.MovimientosLineaDTO;
import py.com.personal.bc.falcon.billing.model.dto.ResponseGeneric;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Arrays;
import java.util.List;

@ApplicationScoped
public class DebitoBusiness {


    private static final Logger logger = LoggerFactory.getLogger(CreditoBusiness.class);


    @Inject
    private LineaBusiness lineaBusiness;

    @Inject
    private BolsaBusiness bolsaBusiness;


    @Inject
    private PrecioBusiness precioBusiness;

    private static final List<String> tipoCredito = Arrays.asList("C", "V", "S", "D");

    //TODO
    @Transactional(transactionMode = TransactionModes.SINGLE_PARTITIONED)
    public ResponseGeneric debitar(MovimientosLineaDTO movimiento) throws Exception {

        if (movimiento == null) {

            logger.error("Movimiento DTO NULL");
            return new ResponseGeneric(false, ErrorsDTO.CODE.NULLVALUES.getValue(), ErrorsDTO.CODE.NULLVALUES.toString());

        }

        if (movimiento.getLinea() == null || movimiento.getCantidad() == null || movimiento.getTipoCredito() == null) {

            logger.error("Null values : linea : {},  cantidad :{}, planId: {} ", movimiento.getLinea(), movimiento.getCantidad(), movimiento.getTipoCredito());

            return new ResponseGeneric(false, ErrorsDTO.CODE.NULLVALUES.getValue(), ErrorsDTO.CODE.NULLVALUES.toString());

        }
        try {

            if (movimiento.getCantidad() < 0D) {

                logger.error("Monto Inválido");

                return new ResponseGeneric(false, ErrorsDTO.CODE.INVALID_AMOUNT.getValue(), ErrorsDTO.CODE.INVALID_AMOUNT.toString() + " : " + movimiento.getCantidad());

            }

            int i = tipoCredito.indexOf(movimiento.getTipoCredito());

            if (i < 0) {

                logger.error("Tipo credito no existe: {}", movimiento.getTipoCredito());

                return new ResponseGeneric(false, ErrorsDTO.CODE.NOEXIST.getValue(), ErrorsDTO.CODE.NOEXIST.toString() + " : " + movimiento.getTipoCredito());

            }

            Linea linea = lineaBusiness.loadBlocking(movimiento.getLinea());


            if (linea == null) {

                logger.error("Linea  no existe: {}", movimiento.getTipoCredito());

                return new ResponseGeneric(false, ErrorsDTO.CODE.NOEXIST.getValue(), ErrorsDTO.CODE.NOEXIST.toString() + " : " + movimiento.getLinea());
            }


            Bolsa bolsa = (linea.getCreditos() != null && !linea.getCreditos().isEmpty()) ? linea.getCreditos().get(i) : null;

            if (bolsa == null) {

                logger.error("Ninguna bolsa  no asignada a la linea: {}", movimiento.getLinea());

                return new ResponseGeneric(false, ErrorsDTO.CODE.NOEXIST.getValue(), ErrorsDTO.CODE.NOEXIST.toString() + " : " + "Bolsa");
            }


            if(i == 0 && canDoDebit(bolsa.getCantidad(), movimiento.getCantidad()) ){

                logger.error("Saldo credito Insuficiente , saldo: {}, movimiento {}", bolsa.getCantidad(), movimiento.getCantidad());

                return new ResponseGeneric(false, ErrorsDTO.CODE.INSUFFICIENT_CREDIT.getValue(), ErrorsDTO.CODE.INSUFFICIENT_CREDIT.toString() + " : " + bolsa.getCantidad());

            }
            switch (i){
                case 0:
                    bolsa.setCantidad(bolsa.getCantidad()-movimiento.getCantidad());
                    break;
                case 1:
                    if( !hasCredit(bolsa.getCantidad())) {

                        break;
                    }
                case 2:
                    break;
                case 3:
                    break;

            }

            bolsaBusiness.update(bolsa);


            return new ResponseGeneric(true, DataDTO.CODE.CREATE_SUCCESS.getValue(), DataDTO.CODE.CREATE_SUCCESS.toString());

        } catch (Exception e) {
            logger.error("Acreditar: {} ", e.getMessage());
            logger.debug("Acreditar: {} ", e);
            throw e;
        }
    }


    private static boolean canDoDebit(Double actual, Double debit){

        return ((actual-debit) < 0D);

    }

    private static boolean hasCredit(Double actual){

        return ( Double.compare(actual,0D) !=0 );

    }

}
