package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.PlanDAO;
import py.com.personal.bc.falcon.billing.dao.PlanDAOTrx;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.exceptions.ObjectAlreadyExist;
import py.com.personal.bc.falcon.billing.model.Plan;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PlanBusiness {

    private static final Logger logger = LoggerFactory.getLogger(PlanBusiness.class);

    @Inject
    private PlanDAO dao;

    @Inject
    private PlanDAOTrx planDAOTrx;

    public Plan loadUnlocking(String linea) throws Exception {

        return dao.select(linea);

    }

    public Plan loadLocking(String linea) throws Exception{

        return planDAOTrx.load(linea);

    }

    public Plan create(String linea, String planId) throws Exception{

        if(linea == null || planId == null)
        {
            logger.error("IdLinea NULL");
            throw new CRUDFail("BOLSA_CREATE : NULL VALUES");

        }
        if (dao.select(linea) != null) {
            logger.error("Error al insertar  , la línea {}  ya existe:", linea);
            throw new ObjectAlreadyExist("Linea: " + linea);
        }

        try {

            Plan plan = new Plan();
            plan.setLinea(linea);
            plan.setPlan(planId);
            planDAOTrx.insert(plan);

            return plan;

        } catch (Exception e) {
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }

    public Plan update(Plan plan) throws Exception{

        if (plan == null) {
            logger.error("Plan -> UPDATE NULL");
            throw new CRUDFail("Updating  Plan-->NULL");
        }

        try {

            planDAOTrx.update(plan);

            return plan;

        } catch (Exception e) {

            logger.error("update plan: {} ", e.getMessage());
            logger.debug("update plan", e);
            throw e;
        }
    }
    public Plan delete(Plan plan) throws Exception{

        if (plan == null) {
            logger.error("Plan -> DELETE NULL");
            throw new CRUDFail("Deleting  Plan-->NULL");
        }

        try {

            planDAOTrx.delete(plan);

            return plan;

        } catch (Exception e) {

            logger.error("delete plan: {} ", e.getMessage());
            logger.debug("delete plan", e);
            throw e;
        }
    }
}
