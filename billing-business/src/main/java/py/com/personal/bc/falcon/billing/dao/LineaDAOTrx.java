package py.com.personal.bc.falcon.billing.dao;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.common.transactions.voltdb.dao.TransactionalDAO;
import py.com.personal.bc.falcon.billing.model.Linea;
import py.com.personal.bc.voltdb.utils.mapper.MapConfig;
import py.com.personal.bc.voltdb.utils.mapper.views.DefaultView;

public class LineaDAOTrx extends TransactionalDAO<Linea> {

    private static final Logger logger = LoggerFactory.getLogger(LineaDAOTrx.class);


    public Linea load(String lineaId) throws Exception {

        Linea linea = new Linea();
        linea.setLinea(lineaId);

        MapConfig config = new MapConfig(Linea.class, DefaultView.class, true);
        config.setLock(true);

        return singlePartitionedLoad(linea, config, lineaId, null);
    }
}
