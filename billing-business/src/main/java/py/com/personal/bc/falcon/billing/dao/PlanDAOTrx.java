package py.com.personal.bc.falcon.billing.dao;

import py.com.personal.bc.common.transactions.voltdb.dao.TransactionalDAO;
import py.com.personal.bc.falcon.billing.model.Plan;
import py.com.personal.bc.voltdb.utils.mapper.MapConfig;

public class PlanDAOTrx extends TransactionalDAO<Plan> {

    public Plan load(String linea) throws Exception {

        Plan plan= new Plan();

        plan.setLinea(linea);

        MapConfig config = new MapConfig(Plan.class, null, true);

        String anyPartition = Math.random() + "";

        return singlePartitionedLoad(plan, config, anyPartition, null);
    }
}
