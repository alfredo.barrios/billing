package py.com.personal.bc.falcon.billing.exceptions;

public class ObjectAlreadyExist extends  Exception {

    public ObjectAlreadyExist(String message) {
        super(message);
    }

}
