package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.HistorialDAO;
import py.com.personal.bc.falcon.billing.dao.HistorialDAOTrx;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.model.Historial;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class HistorialBusiness {

    private static final Logger logger = LoggerFactory.getLogger(HistorialBusiness.class);



    @Inject
    private HistorialDAOTrx dao;



    public Historial create(String linea, String tipoCredito, String proceso, String plan,  String perfilOriginal, String perfilFinal,

                            double creditoOriginal, double creditoFinal, double datosOriginal, double datosFinal, double vozOriginal,
                            double vozFinal, double smsOriginal, double smsFinal, Long transaccion) throws Exception{


        if (linea == null || tipoCredito == null || proceso == null || plan == null
                || perfilOriginal ==null || perfilFinal == null || transaccion == null) {

            logger.error("Linea : {} , tipo credito: {}", linea, tipoCredito);
            throw new CRUDFail("BOLSA_CREATE : NULL VALUES");
        }

        try {

            Historial historial = new Historial();
            historial.setLinea(linea+"-"+System.currentTimeMillis()); // for avoid PK DUPLICATE
            historial.setTipoCredito(tipoCredito);
            historial.setProceso(proceso);
            historial.setPlan(plan);
            historial.setPerfilOriginal(perfilOriginal);
            historial.setPerfilFinal(perfilFinal);
            historial.setCreditoOriginal(creditoOriginal);
            historial.setCreditoFinal(creditoFinal);
            historial.setDatosOriginal(datosOriginal);
            historial.setDatosFinal(datosFinal);
            historial.setVozOriginal(vozOriginal);
            historial.setVozFinal(vozFinal);
            historial.setSmsOriginal(smsOriginal);
            historial.setSmsFinal(smsFinal);
            historial.setTransaccion(transaccion);

            dao.insert(historial);

            return historial;

        } catch (Exception e) {
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }


    public Historial update(Historial historial) throws Exception{

        if (historial == null)
            throw new CRUDFail("Updating historial NULL");

        try {

            dao.update(historial);

            return historial;

        } catch (Exception e) {

            logger.error("update historial: {} ", e.getMessage());
            logger.debug("update historial", e);
            throw e;
        }
    }



    public Historial delete(Historial historial) throws Exception{

        if (historial == null)
            throw new CRUDFail("Deleting historial NULL");

        try {

            dao.delete(historial);

            return historial;

        } catch (Exception e) {

            logger.error("delete historial: {} ", e.getMessage());
            logger.debug("delete historial", e);
            throw e;
        }
    }
}
