package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import py.com.personal.bc.common.transactions.TransactionModes;
import py.com.personal.bc.common.transactions.Transactional;
import py.com.personal.bc.falcon.billing.model.Bolsa;
import py.com.personal.bc.falcon.billing.model.Linea;
import py.com.personal.bc.falcon.billing.model.Plan;
import py.com.personal.bc.falcon.billing.model.dto.DataDTO;
import py.com.personal.bc.falcon.billing.model.dto.ErrorsDTO;
import py.com.personal.bc.falcon.billing.model.dto.ResponseGeneric;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.List;


@ApplicationScoped
public class MiddlewareBusiness {

    private static final Logger logger = LoggerFactory.getLogger(MiddlewareBusiness.class);

    @Inject
    private LineaBusiness lineaBusiness;


    @Inject
    private  PerfilBusiness perfilBusiness;

    @Inject
    private BolsaBusiness bolsaBusiness;

    @Inject
    private HistorialBusiness historialBusiness;

    @Inject
    private PlanBusiness  planBusiness;



    private static final String[] tipoCredito = {"C", "V", "S", "D"};


    @Transactional(transactionMode = TransactionModes.SINGLE_PARTITIONED)
    public ResponseGeneric activate(String lineaId, String perfilId, String planId) throws Exception {

        try {

            if (perfilId == null || lineaId == null || planId ==null) {

                logger.error("Null values : perfilId : {},  lineaId :{}, planId: {} ", perfilId, lineaId, planId);

                return new ResponseGeneric(false, ErrorsDTO.CODE.NULLVALUES.getValue(),ErrorsDTO.CODE.NULLVALUES.toString());


            }

            Linea linea = lineaBusiness.create(lineaId, perfilId);

            logger.info("Created Linea {}",linea);


            Plan plan = planBusiness.create(lineaId, planId);


            logger.info("Created Plan {}",plan);

            for (int i = 0; i < 4; i++)
                bolsaBusiness.create(lineaId, tipoCredito[i]);


            return new ResponseGeneric(true, DataDTO.CODE.CREATE_SUCCESS.getValue(),DataDTO.CODE.CREATE_SUCCESS.toString());


        } catch (Exception e) {
            logger.error("MiddlewareBusiness Activate : {}", e.getMessage());
            logger.debug("MiddlewareBusiness Activate : {}", e);
            throw e;
        }
    }


    @Transactional(transactionMode = TransactionModes.SINGLE_PARTITIONED)
    public ResponseGeneric deactivate(String lineaId) throws Exception{
        try {

            if(lineaId == null){

                logger.error("Null values : lineaId NULL");

                return new ResponseGeneric(false, ErrorsDTO.CODE.NULLVALUES.getValue(),ErrorsDTO.CODE.NULLVALUES.toString());
            }

            Linea linea = lineaBusiness.loadBlocking(lineaId);

            if(linea == null){

                logger.error("MiddlewareBusiness Deactivate : PHONE NUMBER DOES NOT EXIST");

                return new ResponseGeneric(false, ErrorsDTO.CODE.NOEXIST.getValue(), ErrorsDTO.CODE.NOEXIST.toString());

            }

            List<Bolsa> bolsas = bolsaBusiness.loadBlocking(lineaId);

            for(Bolsa b: bolsas)
                bolsaBusiness.delete(b);

            Plan plan = planBusiness.loadLocking(lineaId);

            planBusiness.delete(plan);

            lineaBusiness.delete(linea);

            return new ResponseGeneric(true, DataDTO.CODE.DELETE_SUCCESS.getValue(),DataDTO.CODE.DELETE_SUCCESS.toString());

        }catch (Exception  e){

            logger.error("MiddlewareBusiness Deactivate : {}", e.getMessage());
            logger.debug("MiddlewareBusiness Deactivate : {}", e);
            throw e;
        }
    }
}
