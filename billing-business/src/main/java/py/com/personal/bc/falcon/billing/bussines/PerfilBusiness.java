package py.com.personal.bc.falcon.billing.bussines;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.PerfilDAO;
import py.com.personal.bc.falcon.billing.dao.PerfilDAOTrx;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.exceptions.ObjectAlreadyExist;
import py.com.personal.bc.falcon.billing.model.Perfil;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class PerfilBusiness {


    private static final Logger logger = LoggerFactory.getLogger(PerfilBusiness.class);

    @Inject
    private PerfilDAO perfilDAO;

    @Inject
    private PerfilDAOTrx perfilDAOTrx;


    public Perfil load(String idPerfil) throws Exception {

        return perfilDAOTrx.load(idPerfil);

    }

    public Perfil loadUnlocking(String idPerfil) throws Exception {

        return perfilDAO.select(idPerfil);

    }



    public Perfil create(String perfilId, String credito, String voz, String sms, String datos) throws Exception {


        if (perfilId == null || credito == null || voz == null || sms == null || datos == null) {
            logger.error("perfilId : {} , credito: {}, voz= {}, sms = {} , datos = {}", perfilId, credito, voz, sms, datos);
            throw new CRUDFail("BOLSA_CREATE : NULL VALUES");
        }

        Perfil checkPerfil = perfilDAO.select(perfilId);

        if (checkPerfil == null) {
            logger.error("Perfil Already Exist");
            throw new ObjectAlreadyExist("PerfilId: " + perfilId);
        }

        try {

            Perfil perfil = new Perfil();
            perfil.setPerfil(perfilId);
            perfil.setCredito(credito);
            perfil.setDatos(datos);
            perfil.setSms(sms);
            perfil.setVoz(voz);

            perfilDAO.insert(perfil);

            return perfil;

        } catch (Exception e) {
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }

    public Perfil update(Perfil perfil) throws Exception {

        if (perfil == null) {
            logger.error("Trying to update a NULL Profile");
            throw new CRUDFail("PROFILE_UPDATE : NULL VALUES");

        }
        try {

            perfilDAO.update(perfil);

            return perfil;

        } catch (Exception e) {
            logger.error("update profile: {} ", e.getMessage());
            logger.debug("update profile: ", e);
            throw e;
        }

    }

    public Perfil delete(Perfil perfil) throws Exception {

        if (perfil == null) {

            logger.error("Trying to delete a NULL Profile");
            throw new CRUDFail("PROFILE_DELETE : NULL VALUES");

        }
        try {

            perfilDAO.deleteById(perfil.getPerfil());

            return perfil;

        } catch (Exception e) {
            logger.error("delete profile: {} ", e.getMessage());
            logger.debug("delete profile: ", e);
            throw e;
        }
    }
}
