package py.com.personal.bc.falcon.billing.exceptions;

public class CRUDFail extends Exception {

    public CRUDFail(String message) {
        super(message);
    }

}
