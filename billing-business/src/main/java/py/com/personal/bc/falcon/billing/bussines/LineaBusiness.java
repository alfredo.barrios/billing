package py.com.personal.bc.falcon.billing.bussines;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import py.com.personal.bc.falcon.billing.dao.LineaDAO;
import py.com.personal.bc.falcon.billing.dao.LineaDAOTrx;
import py.com.personal.bc.falcon.billing.exceptions.CRUDFail;
import py.com.personal.bc.falcon.billing.exceptions.ObjectAlreadyExist;
import py.com.personal.bc.falcon.billing.model.Linea;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.util.Date;

@ApplicationScoped
public class LineaBusiness {

    private static final Logger logger = LoggerFactory.getLogger(LineaBusiness.class);


    @Inject
    private LineaDAOTrx dao;

    @Inject
    private LineaDAO lineaDAO;



    public Linea loadBlocking(String lineaId) throws Exception{

        try {


            return dao.load(lineaId);


        }catch (Exception e){
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }

    public Linea create(String idLinea, String perfil) throws Exception {
        if(idLinea == null)
        {
            logger.error("IdLinea NULL");
            throw new CRUDFail("BOLSA_CREATE : NULL VALUES");

        }
        if (lineaDAO.select(idLinea) != null) {
            logger.error("Error al insertar, la línea {}  ya existe:", idLinea);
            throw new ObjectAlreadyExist("Linea: " + idLinea);
        }

        try {

            Linea linea = new Linea();
            linea.setLinea(idLinea);
            linea.setFechaActualizacion(new Date());
            linea.setPerfil(perfil);
            dao.insert(linea);

            return linea;

        } catch (Exception e) {
            logger.error("create: {} ", e.getMessage());
            logger.debug("create ", e);
            throw e;
        }
    }

    public Linea update(Linea linea) throws Exception {

        if (linea == null)
            throw new CRUDFail("Updating Linea NULL");

        try {

            dao.update(linea);

            return linea;

        } catch (Exception e) {

            logger.error("update linea: {} ", e.getMessage());
            logger.debug("update linea", e);
            throw e;
        }
    }

    public Linea delete(Linea linea) throws Exception {


        if (linea == null )
            throw new CRUDFail("Deleting Linea NULL, or does not exist ");

        try {

            dao.delete(linea);

            return linea;

        } catch (Exception e) {
            logger.error("update: {} ", e.getMessage());
            logger.debug("update ", e);
            throw e;
        }
    }
}
