import org.junit.Test;
import org.junit.runner.RunWith;

import py.com.personal.bc.falcon.billing.bussines.MiddlewareBusiness;
import py.com.personal.bc.falcon.billing.bussines.PerfilBusiness;
import py.com.personal.bc.falcon.billing.bussines.PlanBusiness;
import py.com.personal.bc.falcon.billing.model.Perfil;
import py.com.personal.bc.falcon.billing.model.Plan;
import py.com.personal.bc.falcon.billing.model.dto.ResponseGeneric;
import py.com.personal.bc.testingse.runner.CDIJUnitRunner;
import py.com.personal.bc.testingse.util.RequiresThreadScope;

import javax.inject.Inject;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;


@RunWith(CDIJUnitRunner.class)
@RequiresThreadScope
public class ActivacionTest {

    @Inject
    MiddlewareBusiness middlewareBusiness;

    @Inject
    PerfilBusiness perfilBusiness;

    @Inject
    PlanBusiness planBusiness;

    @Test
    public void activation() throws Exception {

        Perfil perfil = perfilBusiness.loadUnlocking("1");

        if (perfil == null)
            perfil = perfilBusiness.create("1", "S", "S", "S", "S");

        Plan plan = planBusiness.loadUnlocking("0981595590");

        if (plan == null)
            plan = planBusiness.create("0981595590", "1");


        ResponseGeneric responseGeneric =middlewareBusiness.activate("0981595590", perfil.getPerfil(), plan.getPlan());

        assertTrue(responseGeneric.isStatus());

    }
}
